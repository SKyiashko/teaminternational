﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculatorApp;
using NUnit.Framework;

namespace CalculatorTest
{
    [TestFixture, Description("Tests for the scientific calculator")]
    class ScientificСalcTest
    {
        ScientificСalc scientificСalc;

        [SetUp]
        public void Init()
        {
            scientificСalc = new ScientificСalc();
        }

        [TearDown]
        public void Clean()
        {
            scientificСalc = null;
        }

        [Test, Order(3)]
        public void SqrtTest()
        {
            Assert.That(scientificСalc.Sqrt(25), Is.EqualTo(5), "Sqrt(25) should be equal to 5");
        }

        [Test, Order(2),Description("Test for degree")]
        public void DegreeTest()
        {
            Assert.That(scientificСalc.Degree(5, 3), Is.EqualTo(125), "5^3 should be equal to 125");
        }

        [Test, Order(1), Description("Test for factorial")]
        public void FactorialTest()
        {
            Assert.That(scientificСalc.Factorial(5), Is.EqualTo(120), "5! should be equal to 120");
        }

        [Test, Description("Test for maximum")]
        public void MaxTest()
        {
            int[] array = { 2, 9, 1, 5, 3 };
            Assert.That(scientificСalc.Max(array), Is.EqualTo(9), "Maximum should be equal to 9");
        }

        [Test, Description("Test for minimum")]
        public void MinTest()
        {
            int[] array = { 2, 9, 1, 5, 3 };
            Assert.That(scientificСalc.Min(array), Is.EqualTo(1), "Minimum should be equal to 1");
        }

        [Test]
        public void SumTest()
        {
            Assert.That(scientificСalc.Sum(2, 2), Is.EqualTo(4), "2 + 2 should be equal to 4");
        }

        [Test]
        public void SubtractionTest()
        {
            Assert.That(scientificСalc.Subtraction(4, 2), Is.EqualTo(2), "4 - 2 should be equal to 2");
        }

        [Test]
        public void MultiplicationTest()
        {
            Assert.That(scientificСalc.Multiplication(2, 2), Is.EqualTo(4), "2 * 2 should be equal to 4");
        }

        [Test, Description("Test for division")]
        public void DivisionTest()
        {
            Assert.That(scientificСalc.Division(5, 2), Is.EqualTo(2.5), "5 / 2 should be equal to 2.5");
        }

        [Test]
        public void DivisionTestExc()
        {
            Assert.Throws<ArgumentException>(delegate { throw new ArgumentException(); });
        }

        [TestCase(2,3, ExpectedResult=5)]
        [TestCase(10, -3, ExpectedResult = 7)]
        [TestCase(5, 5, ExpectedResult = 10)]
        [TestCase(12, 13, ExpectedResult = 25)]

        public double SumTestCase(double a, double b)
        {
            return scientificСalc.Sum(a, b);
        }

        [TestCase(2, 3, ExpectedResult = 6)]
        [TestCase(10, -3, ExpectedResult = -30)] 
        [TestCase(5, 5, ExpectedResult = 25)]
        [TestCase(12, 13, ExpectedResult = 156)]

        public double MultiplicationTestCase(double a, double b) 
        {
            return scientificСalc.Multiplication(a, b);
        }

        [Test]
        public void PassTest()
        {
            Assert.Pass("This test always passes.");
        }

        [Test]
        public void FailTest() 
        {
            Assert.Fail("This test always falls.");
        }


            string phrase = "These are tests for the scientific calculator";


        [Test]

        public void ContainsTest()
        {
            Assert.That(phrase, Contains.Substring("scientific"));
        }

        [Test]
        public void StartWithTest()
        {
            Assert.That(phrase, Does.StartWith("These are"));
        }

        [Test]
        public void AreEqualTest()
        {
            Assert.That(phrase, Is.EqualTo("THESE ARE TESTS FOR THE SCIENTIFIC CALCULATOR").IgnoreCase);
        }

        string[] sarray = new string[] { "These", "are", "tests", "for", "the", "scientific", "calculator", ""};

        [Test]
        public void GreaterTest()
        {
            Assert.Greater(10, sarray[1].Length);
        }

        [Test]
        public void LessOrEqualTest()
        {
            Assert.LessOrEqual("scientific", sarray[5]);
        }

        [Test]
        public void IsEmptyTest()
        {
            Assert.IsEmpty(sarray);
        }

        [Test]
        public void IsEmptyTestString()
        {
            Assert.IsEmpty(sarray[7]);
        }
    }
}
