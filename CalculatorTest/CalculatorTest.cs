﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculatorApp;
using NUnit.Framework;

namespace CalculatorTest
{
    [TestFixture]
    public class CalculatorTest
    {
        Calculator calculator = new Calculator(); 

        [Test]
        public void SumTest()
        {
            Assert.AreEqual(4, calculator.Sum(2, 2), "2 + 2 should be equal to 4"); 
        }

        [Test]
        [Ignore("Ignore the test")]
        public void SumTestNotEqual()
        {
            Assert.AreNotEqual(4, calculator.Sum(2, 2), "2 + 2 should be equal to 4");
        }

        [Test]
        [Repeat(10)]
        public void SubtractionTest()
        {
            Assert.AreEqual(2, calculator.Subtraction(4, 2), "4 - 2 should be equal to 2");
        }

        [Test]
        public void MultiplicationTest()
        {
            Assert.AreEqual(4, calculator.Multiplication(2, 2), "2 * 2 should be equal to 4");
        }

        [Test]
        [Retry(3)]
        public void MultiplicationIntTest()
        {
            Assert.IsInstanceOf(typeof(int), calculator.Multiplication(2, 2), "The variable should be an instance of a double");
        }

        [Test]
        public void DivisionTest()
        {
            Assert.AreEqual(2.5, calculator.Division(5, 2), "5 / 2 should be equal to 2.5");
        }

        [Test]
        public void DivisionTestExc()
        {
            Assert.Throws<ArgumentException>(delegate { throw new ArgumentException(); });
        }
    }
}
