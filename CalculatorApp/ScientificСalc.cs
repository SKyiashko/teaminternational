﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CalculatorApp
{
    public class ScientificСalc : Calculator 
    {
        public double Sqrt(double a) 
        {
            return Math.Sqrt(a); 
        }

        public double Degree(double a, double b)  
        {
            double degree = a;
            for(int i = 1; i < b; i++)
            {
                degree = Multiplication(degree, a); 
            }
            return degree;
        }

        public double Square(double a) 
        {
            return Degree(a, 2);
        }

        public double Factorial(int a) 
        {
            double f = 1; 

            for (int i = 1; i <= a; i++) 
                f *= (double)i; 

            return f;  
        }

        public int Max(int[] array)
        {
            int max = 0;
            foreach(int elem in array)
            {
                if (max < elem) max = elem;
            }
            return max;
        }

        public int Min(int[] array)
        {
            int min = array[0];
            foreach (int elem in array)
            {
                if (min > elem) min = elem;
            }
            return min;
        }
    }
}
